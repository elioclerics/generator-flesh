![](https://elioway.gitlab.io/elioflesh/express-flesh/elio-flesh-express-logo.png)

# <%= kebabName %>

A simple photo gallery with instructions for anyone to build something.

- [express-flesh Documentation](https://elioway.gitlab.io/elioflesh/express-flesh/)

## Seeing is Believing

```
cd <%= kebabName %>
npm i -g @elioway/bones
npm i
bones takeupT <%= kebabName %> --mainEntityOfPage=ImageGallery
bones anonifyT <%= kebabName %>

PHOTOIDENTIFIER=2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG
#fish
set PHOTOIDENTIFIER 2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG

bones takeonT <%= kebabName %> $PHOTOIDENTIFIER --mainEntityOfPage=ImageObject
bones anonifyT $PHOTOIDENTIFIER
bones updateT $PHOTOIDENTIFIER --MediaObject.height=1936 --MediaObject.width=1936 --MediaObject.contentUrl=$PHOTOIDENTIFIER --image=$APPROOT/media/$PHOTOIDENTIFIER
bones cleanT $PHOTOIDENTIFIER
bones readT $PHOTOIDENTIFIER

# Do more photos which are in the `media` folder.

npm run serve
```

Do more! Start here.

- [express-flesh Quickstart](https://elioway.gitlab.io/elioflesh/express-flesh/quickstart.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/express-flesh/apple-touch-icon.png)
