import reload from "reload"
import createServer from "./createServer.js"

let CFG = { PORT: "302x" }
createServer(CFG).then(app => {
  reload(app)
    .then(() =>
      app.listen(CFG.PORT, () =>
        console.log(`<%=kebabName%> DEV server listening on ${CFG.PORT}`)
      )
    )
    .catch(err =>
      console.error(
        "**reload** could not start <%=kebabName%> DEV server.",
        err
      )
    )
})
