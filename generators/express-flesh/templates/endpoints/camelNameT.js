export const <%= camelName %>T = (req, res) => {
  let { app, originalUrl } = req
  let ribs = app.get("ribs")
  let db = app.get("db")
  let cfg = app.get("cfg")
  let { readT, listT } = ribs
  let identifier =
    originalUrl && originalUrl.split("/").filter(b=>!!b).length
      ? originalUrl.split("/").pop()
      : cfg.APPIDENTIFIER
  readT({ identifier }, ribs, db, (readStatusCode, engagedData) => {
    if (readStatusCode !== 200) {
      res.render(readStatusCode.toString(), { 
        ...engagedData,
        ...engagedData.Action,
      })
    } else {
      listT({ identifier }, ribs, db, (_, fullListOfThings) => {
        /** Weird, right? This is the `readT` endpoint! Why call `listT`?
         * @tutorial
         * The user/developer might like to add some of the engaged Thing's list on this page. Therefore
         * it's important get the full list out of the database, not the local one inside engagedData
         * which may have minified versions (or list the list if identifiers), depending on the DB system.
         */
        res.render("<%= camelName %>T", {
          ...engagedData,
          // See above for why we add the full list to the `readT` endpoint template.
          ItemList: {
            itemListElement: fullListOfThings,
            numberOfItems: fullListOfThings.length,
          },
        })
      })
    }
  })
}

export default <%= camelName %>T
