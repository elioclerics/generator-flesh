"use strict"
const Thingrator = require("generator-thing/generators/thingrator")

// NB: This can't be tested.  It calls default.
module.exports = class Appitator extends Thingrator {
  initializing() {
    this._eliosay("express-flesh")
    this._blackquote(`-`)
    this.composeWith("flesh:express-flesh", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: Appitator,
    })
  }
}
