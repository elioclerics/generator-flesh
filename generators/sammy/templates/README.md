![](https://elioway.gitlab.io/elioflesh/elio-thing-logo.png)

# <%= name %>

> Can you get this done by Wednesday? **Rosalind Codrington**

An SinglePageApp consuming the bones REST API - to put some flesh on dem bones.

## Requirements

- [mongo server](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

## Dev Steps

```bash
mkdir <%= name %>
cd <%= name %>
yo flesh
gulp
```

## Credits

### Core! Ta Very Much

- <http://www.sammyjs.org/docs/tutorials/json_store_1>
- <http://www.sammyjs.org>

### Other

- [https://commons.wikimedia.org/wiki/File:The*Penitent_Magdalen*(Cecco_del_Caravaggio)](<https://commons.wikimedia.org/wiki/File:The_Penitent_Magdalen_(Cecco_del_Caravaggio)>>>)

## License

[MIT](LICENSE) © [Tim Bushell]()

![](apple-touch-icon.png)
