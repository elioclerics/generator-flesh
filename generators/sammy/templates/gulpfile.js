// generated on <%= date %>
var browserSync = require("browser-sync").create()
var gulp = require("gulp"),
  clean = require("gulp-clean-css"),
  concat = require("gulp-concat"),
  rename = require("gulp-rename"),
  sass = require("gulp-sass")
var nodemon = require("nodemon")

// Load the bones server in the background
gulp.task("server", function () {
  // configure nodemon
  nodemon({
    // the script to run the app
    script: "node_modules/@elioway/bones/bones/server.js",
    // this listens to changes in any of these files/routes and restarts the application
    watch: [],
    ext: "js",
    // Below i'm using es6 arrow functions but you can remove the arrow and have it a normal .on('restart', function() { // then place your stuff in here }
  }).on("restart", () => {
    gulp
      .src("node_modules/@elioway/bones/bones/server.js")
      // I've added notify, which displays a message on restart. Was more for me to test so you can remove this
      .pipe(notify("Running the start tasks and stuff"))
  })
})

// Compile sass into CSS & auto-inject into browsers
var sass_watch = ["stylesheets/*.scss", "stylesheets/**/*.scss"]
var css_folder = "css/"
gulp.task("sass", function () {
  return gulp
    .src("stylesheets/judge.scss")
    .pipe(
      sass({
        includePaths: sass_watch,
      })
    )
    .pipe(concat("<%= name %>.css"))
    .pipe(gulp.dest(css_folder))
    .pipe(rename("<%= name %>.min.css"))
    .pipe(clean())
    .pipe(gulp.dest(css_folder))
    .pipe(browserSync.stream())
})

// Minify css into CSS & auto-inject into browsers
var distro_css = ["css/normalize.css", "css/main.css", "css/<%= name %>.css"]
var dcss_folder = "dist/css/"
gulp.task("clean", function () {
  return gulp
    .src(distro_css)
    .pipe(concat("<%= name %>.min.css"))
    .pipe(clean())
    .pipe(gulp.dest(dcss_folder))
    .pipe(browserSync.stream())
})

// Static Server + watccing scss/html files
gulp.task("watch", ["sass", "clean"], function () {
  browserSync.init({
    server: "./",
  })
  gulp.watch(sass_watch, ["sass", "clean"])
  gulp.watch(sass_watch).on("change", browserSync.reload)
  gulp.watch(["*.html"]).on("change", browserSync.reload)
})

gulp.task("default", ["server", "watch"])
