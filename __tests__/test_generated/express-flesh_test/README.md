![](https://elioway.gitlab.io/elioflesh/express-flesh/elio-flesh-express-logo.png)

# express-flesh-test

A simple photo gallery with instructions for anyone to build something.

- [express-flesh Documentation](https://elioway.gitlab.io/elioflesh/express-flesh/)

## Seeing is Believing

```
cd express-flesh-test
npm i -g @elioway/bones
npm i
bones takeup express-flesh-test --mainEntityOfPage=ImageGallery
bones anonifyT express-flesh-test

PHOTOIDENTIFIER=2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG
#fish
set PHOTOIDENTIFIER 2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG

bones takeonT express-flesh-test $PHOTOIDENTIFIER --mainEntityOfPage=ImageObject
bones anonifyT $PHOTOIDENTIFIER
bones updateT $PHOTOIDENTIFIER --MediaObject.height=1936 --MediaObject.width=1936 --MediaObject.contentUrl=$PHOTOIDENTIFIER --image=$APPROOT/media/$PHOTOIDENTIFIER
bones cleanT $PHOTOIDENTIFIER
bones readT $PHOTOIDENTIFIER

# Do more photos which are in the `media` folder.

npm run serve
```

Do more! Start here.

- [express-flesh Quickstart](https://elioway.gitlab.io/elioflesh/express-flesh/quickstart.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/express-flesh/apple-touch-icon.png)
