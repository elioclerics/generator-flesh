import createServer from "./createServer.js"

let CFG = { PORT: "502x" }
createServer(CFG).then(app => {
  app.listen(CFG.PORT, () =>
    console.log(`express-flesh-test server listening on ${CFG.PORT}`)
  )
})

export default createServer
