# Deploy express-flesh-test

## Server setup

```
# keep it up to date
sudo apt update
sudo apt upgrade -y

# keep it up to release
sudo do-release-upgrade
```

## pm2

- <https://pm2.keymetrics.io/docs/usage/quick-start/>

```
sudo npm install pm2@latest -g

pm2 restart funtyper-api
pm2 reload funtyper-api
pm2 stop funtyper-api
pm2 delete funtyper-api

pm2 [list|ls|status]

pm2 monit

# You might not need this.
pm2 startup systemd
```

## GIT clone

```shell
cd ~/tew/
git clone git@gitlab.com:test_generated/express-flesh-test.git
cd express-flesh-test

```

## Sites-enabled

Copy over the config.

```shell
cd ~/tew/express-flesh-test
sudo cp web.conf /etc/nginx/sites-available/express-flesh-test.conf
npm run pm2
```

Enable the site

```shell
sudo ln -s /etc/nginx/sites-available/express-flesh-test.conf /etc/nginx/sites-enabled/express-flesh-test.conf
```

## nginx

```shell
# check for errors in config
sudo nginx -t
# restart
sudo systemctl restart nginx
# logs
systemctl status nginx.service
journalctl -xe
```

## SSL

```
sudo apt install certbot python3-certbot-nginx
sudo ufw allow 'Nginx Full'
sudo ufw delete allow 'Nginx HTTP'

sudo certbot --nginx -d ns388405.ip-176-31-253.eu
```

Answer questions and verify changes to config then...

```
sudo systemctl status certbot.timer
# test
sudo certbot renew --dry-run
```
