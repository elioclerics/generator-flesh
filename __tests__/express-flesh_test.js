"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

describe("generator-thing:express-flesh", () => {
  beforeAll(() => {
    return helpers
      .run(path.join(__dirname, "../generators/express-flesh"))
      .inDir(path.join(__dirname, "test_generated", "express-flesh_test"))
      .withPrompts({ identifier: "express-flesh", subjectOf: "eliomaster" })
  })

  it("creates files", () => {
    assert.file("bin/index.js")
    assert.file("doc/deploy.md")
    assert.file("media/2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG")
    assert.file(
      "media/2011-01-20-gecko-theatre-lamai-koh-samui-thailand-9155299685.JPG"
    )
    assert.file("media/2011-02-02-will-the-last-person-to-leave-9157644222.JPG")
    assert.file("media/2011-02-14-sound-crew-9157760690.JPG")
    assert.file(
      "media/2011-05-02-the-boat-that-was-afraid-of-the-rain-9158743350.JPG"
    )
    assert.file("media/2011-05-16-protection-9156598937.JPG")
    assert.file("media/2011-05-21-little-tree-on-tab-kaek-beach-9157780212.JPG")
    assert.file("package.json")
    assert.file("README.md")
    assert.file("createServer.js")
    assert.file("serve.js")
    assert.file("server-reload.js")
    assert.file("views/404.handlebars")
    assert.file("views/expressFleshTestT.handlebars")
    assert.file("views/layouts/express-flesh-test.handlebars")
    assert.file("views/partials/Express-flesh_test.handlebars")
  })
})
