# generator-flesh Credits

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Artwork

- <https://commons.wikimedia.org/wiki/File:Adam_and_Eve_Chased_out_of_the_Terrestrial_Paradise_(Jean-Achille_Benouville).jpg>
